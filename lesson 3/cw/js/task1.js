/*
  Задание:

    Написать скрипт, который будет добавлять введеный в инпут Receiver текст в список list.
    Если в списке больше 5 записей, убирать Event с кнопки AddToList.
    Кнопке AddToList добавить disabled = true

    После каждого ввода очищать значение инпута

    Используемые методы:

    addEventListener
    removeEventListener

    document.getElementById
    document.createElement

    element.innerText
    element.setAttribute
    element.appendChild

    Получить значение инпута: Receiver.value [Receiver - это id]
*/

function createElement(tag, text, type = '') {
  let el = document.createElement(tag, text);
  el.className = 'list-item';
  el.textContent = text;
  if(type === 'error') {
    el.style.color = 'red';
  }

  return el;
}

const addBtn = document.getElementById('AddToList');

function toDo() {
  let inputField = addBtn.previousElementSibling;
  let targetBlock = document.getElementById('list');
  let errorBlock = document.getElementById('error');
  let errorMsg = '';

  // clear error block from last messages
  if(errorBlock.hasChildNodes()) {
    errorBlock.innerHTML = '';
  }

  // add list item
  if(inputField.value) {
    targetBlock.appendChild(createElement('li', inputField.value));
    inputField.value = '';
    
  } else {
    errorMsg = 'This field must not be empty. Please, enter some value';
    errorBlock.appendChild(createElement('p', errorMsg, 'error'));
  }

  // check child quanity of target block
  if(targetBlock.children.length == 5) {
    errorMsg = 'It was last your record';
    errorBlock.appendChild(createElement('p', errorMsg, 'error'));
    inputField.readOnly = true;
    addBtn.removeEventListener('click', toDo);
  }
}

addBtn.addEventListener('click', toDo);