var blocks = document.querySelectorAll('.roadMap__container');

function moveBall(e) {
  let parent, ballsBlock, firstChildId, lastChildId, curItem;

  parent = e.target.parentNode; // control button and ball's container 
  ballsBlock = parent.querySelector('.roadMap'); // ball's container
  firstChildId = ballsBlock.firstElementChild.dataset.id; // first ball in container
  lastChildId = ballsBlock.lastElementChild.dataset.id; // last ball in container
  balls = parent.querySelectorAll('.roadMapPoint'); // like-array object of ball elements
  
  // receive current ball element which contains class
  for(ball of [...balls]) {
    if(ball.classList.contains('ball')) {
      curItem = ball;
      break;
    }
  }
  curItem.classList.remove('ball');

  // adding ball class to next or previous ball element
  if(e.target.dataset.direction === ">") {
    if (curItem.dataset.id !== lastChildId) {
      nextItem = curItem.nextElementSibling;
    } else {
      nextItem = curItem.parentNode.firstElementChild;
    }
    nextItem.classList.add('ball');
  }
  else {
    if(curItem.dataset.id !== firstChildId) {
      prevItem = curItem.previousElementSibling;
    } else {
      prevItem = curItem.parentNode.lastElementChild;
    }
    prevItem.classList.add('ball');
  }

}

// add event listener to control buttons
blocks.forEach(function(block) {
  let ctrlBtns = block.querySelectorAll('.navButton');

  ctrlBtns.forEach(function(ctrlBtn) {
    block.addEventListener('click', moveBall);
  });
});