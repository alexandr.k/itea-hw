/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/

function createEl() {
  let heading = document.createElement('h1');
      heading.textContent = 'I know how binding works';
      document.body.appendChild(heading);
}

function ChangeColor(bg) {
  this.background = bg;

  document.body.style.background = this.background;
  document.body.style.color = this.color;
  
  createEl();
}

//ChangeColor.call(style, 'yellow');

function SetColor() {
  document.body.style.background = this.background;
  document.body.style.color = this.color;
  
  createEl();
}

var style = {
  color: 'red'
}

var style1 = {
  background: 'orange',
  color: 'blue'
}

// var bindSetColor = SetColor.bind(style1);
// bindSetColor();

function ApplySetColor(text) {
  document.body.style.background = this.background;
  document.body.style.color = this.color;

  let heading = document.createElement('h1');
  heading.textContent = text;
  document.body.appendChild(heading);
}

var params = ['I know how binding works'];
ApplySetColor.apply(style1, params);