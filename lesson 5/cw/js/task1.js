/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

var train = {
    name: 'NNM',
    speed: 0,
    passengers: 0,
    go: function(speed = this.speed) {
        this.speed = speed;
        if(this.speed === 0) {
            console.log('Unfortunately train is stop');
        } else {
            console.log(`Train ${this.name} is carrying ${this.passengers} passenger${(this.passenges == 1) ? '' : 's'} with speed of ${this.speed} km/h`);
        }
    },
    stop: function() {
        this.speed = 0;
        console.log(`Train ${this.name} was stoped. Speed of train is ${this.speed} km/h`);
    },
    takePassengers: function(passengers) {
        
        return this.passengers += passengers;
    }
}

train.name = 'Tarpan';
train.go(60);
train.stop();
train.takePassengers(30);
train.go(50);
