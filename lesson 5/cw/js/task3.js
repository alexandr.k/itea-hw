/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/
// Dog {
//   name: '',
//   breed: '',
//   status: '',
//   run: function() {...},
//   showProperties: function() {...}
// }

function Dog(name, breed, status) {
  this.name = name;
  this.breed = breed,
  this.status = status;
  this.aboutDog = function() {
    for(let key in this) {
      console.log(`${key}: ${this[key]}`);
    }
  };
  this.changeStatus = function(status) {
    this.status = status;
    console.log(`${this.name} is ${this.status}.`);
  }

}

const firstDog = new Dog('Chucha', 'French bulldog', 'live');
firstDog.aboutDog();