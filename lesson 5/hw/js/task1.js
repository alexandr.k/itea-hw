/*

Задание:

  1. Написать конструктор объекта комментария который принимает 3 аргумента
  ( имя, текст сообщения, ссылка на аватаку);

  {
    name: '',
    text: '',
    avatarUrl: '...jpg'
    likes: 0
  }
    + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
    + В прототипе должен быть метод который увеличивает счетик лайков

  var myComment1 = new Comment(...);

  2. Создать массив из 4х комментариев.
  var CommentsArray = [myComment1, myComment2...]

  3. Создать функцию конструктор, которая принимает массив коментариев.
    И выводит каждый из них на страничку.

  <div id="CommentsFeed"></div>

*/

const commentFeed = document.getElementById('CommentsFeed');

function Comment(name, msg, avaLink) {
  this.name = name;
  this.message = msg;
  this.like = 0;
  if(avaLink) {
    this.avatarLink = avaLink;
  }
}

Comment.prototype.avatarLink = 'def_cat.png';
Comment.prototype.likeCount = function()  {
  this.like++;
}

function renderComments(comments) {
  let commentBlock = '';
  let imgPath = './images/';

  for(let comment of comments) {
    commentBlock += `
      <div class="commentBlock">
        <div class="userAva">
          <img src="${imgPath}${comment.avatarLink}" />
        </div>
        <div class="commentInner">
          <p class="name">${comment.name}</p>
          <p class="commentText">${comment.message}</p>
        </div>
      </div>
    `;
  }

  return commentBlock;
}



var commentsArray = [];

commentsArray[0] = new Comment('Petr', 'Hello', '');
commentsArray[1] = new Comment('Vasiliy', 'Hello. My name is Vasiliy', 'coala.png');
commentsArray[2] = new Comment('Vladimir', 'Hello. Where are you from', 'lemur.png');


commentFeed.innerHTML = renderComments(commentsArray);






