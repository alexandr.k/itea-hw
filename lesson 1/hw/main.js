/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    */
    var stylesEl = 'height: 300px; left: 50%; margin: -150px 0 0 -150px; position: absolute; top: 50%; width: 300px;';

    function genRandomNum(min, max) {
      min = Math.floor(min);
      max = Math.ceil(max);
      return min + Math.floor(Math.random() * (max + 1 - min));
    }

    // generate rgb code color
    function genRgbCol(min, max) {
      let rgb = [], rgbCol;
      for(let i = 0; i < 3; i++) {
        rgb[i] = genRandomNum(min, max);
      }
      rgbCol = 'rgb(' + rgb.join(', ') + ')';
      
      return rgbCol;
    }


    // change color function by click on button
    function changeCol() {
      let el = document.querySelector('#box-el');
      el.style.background = genRgbCol(0, 255);
    }

    // create custom block
    let block = document.createElement('div');
    block.setAttribute('style', stylesEl);
    block.id = "box-el";

    // create button to change color by click
    let btn = document.createElement('button');
    btn.setAttribute('onclick', 'changeCol(event)');
    btn.textContent = 'Change color';

    // insert elements to html structure
    document.getElementById('app').appendChild(block);
    document.getElementById('app').appendChild(btn);


    /*
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/
  // convert color function from rgb to hex
  function rgbToHex() {
    let hexNum, hexVal, hexArr = [];
    for(let i = 0; i < 3; i++) {
      hexNum = genRandomNum(0, 255).toString(16);
      if (hexNum.length < 2) {
        hexNum = '0' + hexNum;
      }
      hexArr[i] = hexNum;
    }
    return '#' + hexArr.join('');
  }


  blockEl = document.getElementById('box-el');
  // for first task
  //blockEl.style.background =  genRgbCol(0, 255);
  
  // for second task
  blockEl.style.background = rgbToHex();