/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Array



  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>

    <button></button>
  </form>


    <input />

  -> '{"name" : !"23123", "age": 15, "password": "*****" }'

*/

const form = document.forms[0];
const elements = form.elements;
const jsonRes = document.getElementById('jsonResult')
var json = {};

elements.send.onclick = function(e) {
  e.preventDefault();

  [...elements].forEach(function(element){
    if(element.localName == 'input') {
      console.log(element.name);
      json[element.name] = element.value;
    }
  });

  let dataString = JSON.stringify(json);
  console.log(dataString);
  form.reset();
};

jsonRes.addEventListener('change', function() {
  let jsonData = jsonRes.value;
  console.log(JSON.parse(jsonData));
})