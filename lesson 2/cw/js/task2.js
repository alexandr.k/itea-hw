/*

Поработаем немного с ивентами:
onmouseenter
onclick
oncontextmenu
onkeydown

Задание:
На каждую из конопок назначить действие которое будет выполнять следующее действие:
1.На ховер(onmouseenter) повесить обработчик который будет при каждом наведении
  на кнопку будет выводить в блок результатом елемент li с текстом "hovered!"
  у которого будет один из следующих класов:
  red blue green orange purple

2.На кнопку клика(onclick) повесить обработчик который будет удалять классы с
  блока с результатом по очереди в таком порядке:

  six > two > three > four > one > five

  после того как блок окажется пустым, добавить их в порядке возрастания

  one > two > three ... > six

3.На кнопку контекстного меню (oncontextmenu) повесить обработчик который в блок
  для результата выведет блок со списком меню используя следующий шаблон:

  <a href="http://google.com.ua">Google</a>
  <a href="http://itea.ua/">ITEA</a>
  <a href="https://www.youtube.com/">Youtube</a>

  для запрета вызова контектстного меню, нужно в функцию обработчик передать событие
  element.onevent = function(e){...}
  и вызвать внутри этой функции из этой переменной
  e.preventDefault(); <- Отмена действия по умолчанию

  Повтороное кажатие на эту кнопку правой или левой кнопкой мыши должно очистить блок.

4. На кнопку с обработчиком нажатия (onkeydown) повесить обработчик который будет
  Выводить число нажатий на любую из клавиш клавиатуры


*/

/* var mouseHover = document.getElementById('mouseHover');
var mouseClick = document.getElementById('mouseClick');
var mouseContext = document.getElementById('mouseContext');
var keyboardButton = document.getElementById('keyboardButton');

mouseHover.onmouseenter = function(){
  console.log('hover');
}

var clickIndex = 0;
var direction = true;

mouseClick.onclick = function(){
    console.log('click');
}
var showContext = false;
mouseContext.oncontextmenu = function(e){
    e.preventDefault();
    console.log('context');
}

var counter = 0;
keyboardButton.onkeydown = function(){
    console.log('keydown');
} */


/* First block */
var colorClass = ['red', 'blue', 'green', 'orange', 'purple'];
const mouseHover = document.getElementById('mouseHover');
const hoverRes = document.getElementById('hoverResult');

function randomNum() {
  return Math.floor(Math.random() * 5);
}

mouseHover.onmouseenter = function() {
  let li = document.createElement('li');
  li.className = colorClass[randomNum()];
  li.textContent = 'Hovered!';
  hoverRes.append(li);
}

/* Second block */

var counter = 0;
var classes = ['six','two', 'three', 'four', 'one', 'five'];
var sortedClasses = ['one', 'two', 'three', 'four', 'five', 'six'];

const mouseClick = document.getElementById('mouseClick');
const clickRes = document.getElementById('clickResult');

mouseClick.onclick = function() {
  const classAtr = classes[counter];
  clickRes.classList.remove(classAtr);
  counter++;
}

/* Third block */
var click = 0;
const mouseCont = document.getElementById('mouseContext');
const contextLinks = '<a class="contextLink" href="http://google.com.ua">Google</a><a class="contextLink" href="http://itea.ua/">ITEA</a><a class="contextLink" href="https://www.youtube.com/">Youtube</a>';

mouseCont.oncontextmenu = function(e) {
  e.preventDefault();  
  if(click % 2 == 0) {
    mouseCont.nextElementSibling.innerHTML = contextLinks;
  } else {
    mouseCont.nextElementSibling.innerHTML = '';
  }
  click++;
}


/* Fourth block */
var keyDown = 0;
const keyboardBtn = document.getElementById('keyboardButton');

keyboardBtn.onkeydown = function() {
  keyDown++;
  // let span = document.createElement('span');
  // span.textContent = keyDown;
  keyboardBtn.nextElementSibling.innerHTML = `<span>${keyDown}</span>`;
}
