/*
  1. Выбрать блок Target
  2. Cоздать див внутри этого блока
  3. Добавить ему 3 класса "Ctrl Shift Delete" используя свойство className
  4. Добавиь класс через метод add
  5. Удалить класс "Delete"
  6. Применить метод toggle на класс "Command"
*/
var target = document.getElementById('target');
var div = document.createElement('div');
div.className = 'Ctrl Shift Delete';
div.classList.add('new-class');
div.classList.remove('Delete');
div.classList.toggle('Command');
target.appendChild(div);