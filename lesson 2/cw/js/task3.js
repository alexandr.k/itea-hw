/*
  Задача: Сделать корректно работающее всплывающее окно.
  Условие: Изменять HTML разметку выше - нельзя.
  При нажании на одну из кнопок, должно открытся окно с соответствующим data- аттрибутом.
  При нажатии на кнопку close окно дожно закрываться.
*/


const btns = document.getElementsByClassName('popup__button');
const popupItems = document.getElementsByClassName('popup__item');
const closePopup = document.getElementById('popup__close');

[...btns].forEach(function(el) {
  el.addEventListener('click', function(e) {
    let popupId = e.target.dataset.popup;
    document.getElementById('popup').classList.add('show');
    
    [...popupItems].forEach(function(popupItem) {
      let popitemId = popupItem.dataset.popup;
      if(popupId === popitemId) {
        popupItem.classList.add('show');
      }
    });
  });
});

closePopup.addEventListener('click', function() {
  document.getElementById('popup').classList.remove('show');
  
  [...popupItems].forEach(function(popupItem) {

    if(popupItem.classList.contains('show')) {
      popupItem.classList.remove('show');
    }
  });
});
