/*
  Задание:
    Сделать валидацию формы:
    1. Логин / Пароль не может быть пустым
    2. Пароли должны совпадать
    3. Пока не стоит что юзер согласен с лиц. соглашением - кнопка отправки не активна

    + бонус - то поле, которое не прошло валидацию подсвечивать красным.
            - которое прошло - подсвечивать зеленым.
*/

var form = document.forms.MyValidateForm;
var login = form.elements.login;
var pass = form.elements.pas1;
var confPass = form.elements.pas2;
var inputs = form.children;

function removeClass(elem) {
  if(elem.classList.contains('error')) {
    elem.classList.remove('error');
  }
}

for(input of inputs) {
  if (input.hasAttribute('name')) {
    input.onfocus = function() {
      removeClass(this);
    }
  } else {
    continue;
  }
}

agree.click = function() {
  if(agree.checked) {
    submit.disabled = false;
  } else {
    submit.disabled = true;
  }
}

submit.onclick = function(e) {
  e.preventDefault();

  let errors = [], index = 0;
  const regWrong = 'Please try to fill fields again';
  const regSuccess = 'Congratulation. You\'ve registered successfully';
  
  for(const input of inputs) {
    if(input.value === '') {
      input.className = 'error';
      errors[index] = 'error';
      index++;
    }
  }

  if(pass.value !== confPass.value) {
    pass.classList.add('error');
    confPass.classList.add('error');
    errors.push('error');
  }

  if(errors.includes('error')) {
    alert(regWrong);
  } else {
    console.log('%cSending data', 'font-size: 19px; color: red;', '\nlogin: ', login.value, 'pass: ', pass.value, 'Confirm pass: ', confPass.value);
    form.reset();
    alert(regSuccess);
  }
  
}