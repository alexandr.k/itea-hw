/*
  Создайте функцию, которая по клику на "показать день недели" возвращает день недели для выбранной даты с инпута.
  День нужно возвратить в div#result в текстовом формате т.е понедельник, вторник, суббота и т.д
*/

var dateField = document.body.querySelector('#MyDateSelector');
var btn = document.body.querySelector('#ShowDayButton');
var resultBlock = document.body.querySelector('#result');
var weeksDay = [
    'Понеділок / Monday',
    'Вівторок / Tuesday',
    'Середа / Wednesday',
    'Четвер / Thursday',
    'П\'ятниця / Friday',
    'Субота / Saturday',
    'Неділя / Sunday'
];

btn.addEventListener('click', function() {
  if(dateField.value) {
    let date = new Date(dateField.value);
    let daysName = weeksDay[date.getDay() - 1];
    resultBlock.innerHTML = `<span class="day">${daysName}</span>`;
  } else {
    resultBlock.innerHTML = `<span class="error">Оберіть будь-ласка дату</span>`;
  }
});

dateField.addEventListener('click', () =>{
  if(resultBlock.children.length > 0) {
    resultBlock.innerHTML = '';
  }
});