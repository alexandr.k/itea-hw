/*
  Создайте счетчик секунд, который начинает отчет с 1 и заканчивается на 30,
  также добавьте кнопки который останавливают отсчет, и запускают его заново
   и добавьте кнопку обнуляющую отчет.
  Также попробуйте изменить код так чтобы отчет начинался с 30 и заканчивался на 1.

  + бонус: Сделать так что бы на каждый тик (1с/2c) менялся цвет фона. Можно на основе генератора случайного цвета с предудущих уроков.
  + бонус: Сделать визуализацию стрелки которая идет по кругу
      document.getElementById("myDIV").style.transform = "rotate(7deg)";

  + бонус: Сделать кнопки которые выбирают режим в котором идет отсчет - обычный от 0 до 30 или реверсивный от 30 до 0
*/

var counter, degree;
const startTimer = document.getElementById('start');
const stopTimer = document.getElementById('stop');
const reverseTimer = document.getElementById('reverse');
const secArrow = document.querySelector('.second-arrow');
const timerField = document.getElementById('timerValue');
const timerBlock = document.body.querySelector('.timer');

function genRgb() {
  let colorsValue = [];
  for(let i = 0; i < 3; i++) {
    colorsValue[i] = Math.floor(Math.random() * 255 + 1);
  }
  [r, g, b] = colorsValue;

  return `rgb(${r}, ${g}, ${b})`;
}

function timer() {
  stop();
  counter = parseInt(timerField.textContent);  
  let intervalId = setInterval(function() {
    (counter !== 30) ? counter++ : counter = 1;
    degree = counter * 12 + 90;
    secArrow.style.transform = `rotate(${degree}deg)`;
    timerField.innerHTML = '';
    timerField.append(counter);
    timerBlock.style.background = genRgb();
  }, 1000);
  timerField.dataset.id = intervalId;
}

function reverse() {
  stop();
  counter = parseInt(timerField.textContent);
  let intervalId = setInterval(function() {
    (counter !== 1) ? counter-- : counter = 30;
    degree = counter * 12 + 90;
    secArrow.style.transform = `rotate(${degree}deg)`;
    timerField.innerHTML = '';
    timerField.append(counter);
    timerBlock.style.background = genRgb();
  }, 1000);
  timerField.dataset.id = intervalId;
}

function stop() {
  clearInterval(timerField.dataset.id);
}

startTimer.addEventListener('click', timer);
reverseTimer.addEventListener('click', reverse);
stopTimer.addEventListener('click', stop);
